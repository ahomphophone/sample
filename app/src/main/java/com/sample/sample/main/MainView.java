package com.sample.sample.main;


import com.sample.sample.models.HockeyPlayer;

import java.util.List;

interface MainView {

    void showRoster(List<HockeyPlayer> roster);
    void showErrorMessage();
    void showProgressDialog();
    void dismissProgressDialog();

}
