package com.sample.sample.main;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sample.sample.R;
import com.sample.sample.models.HockeyPlayer;

import java.lang.reflect.Type;
import java.util.List;

import static com.sample.sample.main.HockeyPlayerAdapter.*;


public class MainActivity extends AppCompatActivity implements MainView, OnHockeyPlayerClickedListener {

    private static final String TAG_HOCKEY_PLAYER_DETAIL_FRAGMENT = "hockey_player_detail_fragment";
    private static final String KEY_ROSTER = "roster";

    private ProgressDialog progressDialog;
    private HockeyPlayerAdapter hockeyPlayerAdapter;
    private MainPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView rosterRecyclerView = (RecyclerView) findViewById(R.id.rosterRecyclerView);
        rosterRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        hockeyPlayerAdapter = new HockeyPlayerAdapter(this);
        rosterRecyclerView.setAdapter(hockeyPlayerAdapter);

        presenter = new MainPresenter(this);

        if (savedInstanceState != null) {
            String rosterJson = savedInstanceState.getString(KEY_ROSTER);
            Type listType = new TypeToken<List<HockeyPlayer>>() {}.getType();
            List<HockeyPlayer> roster = new Gson().fromJson(rosterJson, listType);
            showRoster(roster);

        } else {
            presenter.init();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        presenter.unbind();
        presenter = null;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        Gson gson = new Gson();
        String rosterJson = gson.toJson(hockeyPlayerAdapter.getRoster());

        outState.putString(KEY_ROSTER, rosterJson);
    }

    private void showHockeyPlayerDetail(HockeyPlayer hockeyPlayer) {
        Gson gson = new Gson();
        String hockeyPlayerJson = gson.toJson(hockeyPlayer);

        HockeyPlayerDetailFragment hockeyPlayerDetailFragment =
                HockeyPlayerDetailFragment.newInstance(hockeyPlayerJson);

        FragmentManager fragmentManager = getSupportFragmentManager();
        hockeyPlayerDetailFragment.show(fragmentManager, TAG_HOCKEY_PLAYER_DETAIL_FRAGMENT);
    }

    //==============================================================================================
    // MainView Implementation
    //==============================================================================================

    @Override
    public void showRoster(List<HockeyPlayer> roster) {
        hockeyPlayerAdapter.setRoster(roster);
    }



    @Override
    public void showErrorMessage() {
        Toast.makeText(this, R.string.roster_failed_message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showProgressDialog() {
        progressDialog = new ProgressDialog(this);
        String loadingMessage = getString(R.string.loading);
        progressDialog.setMessage(loadingMessage);
        progressDialog.show();
    }

    @Override
    public void dismissProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    //==============================================================================================
    // OnHockeyPlayerClickedListener Implementation
    //==============================================================================================

    @Override
    public void onHockeyPlayerClicked(HockeyPlayer hockeyPlayer) {
        showHockeyPlayerDetail(hockeyPlayer);
    }

}
