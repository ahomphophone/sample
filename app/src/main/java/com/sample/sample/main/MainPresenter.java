package com.sample.sample.main;


import com.sample.sample.managers.RosterManager;
import com.sample.sample.models.HockeyPlayer;

import java.util.List;

class MainPresenter {

    private MainView view;

    MainPresenter(MainView view) {
        this.view = view;
    }

    void init() {
        view.showProgressDialog();

        RosterManager rosterManager = new RosterManager();
        rosterManager.fetchRosters(new RosterManager.FetchRosterCallback() {
            @Override
            public void onSuccess(List<HockeyPlayer> roster) {
                if (view != null) {
                    view.showRoster(roster);
                    view.dismissProgressDialog();
                }
            }

            @Override
            public void onError() {
                if (view != null) {
                    view.showErrorMessage();
                    view.dismissProgressDialog();
                }
            }
        });
    }

    void unbind() {
        view = null;
    }
}
