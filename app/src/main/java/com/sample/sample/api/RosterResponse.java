package com.sample.sample.api;


import com.sample.sample.models.HockeyPlayer;

import java.util.List;

public class RosterResponse {

    public List<HockeyPlayer> roster;

}
