package com.sample.sample.managers;


import com.sample.sample.api.RosterApi;
import com.sample.sample.api.RetrofitService;
import com.sample.sample.api.RosterResponse;
import com.sample.sample.models.HockeyPlayer;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RosterManager {

    public interface FetchRosterCallback {
        void onSuccess(List<HockeyPlayer> roster);
        void onError();
    }

    public void fetchRosters(final FetchRosterCallback callback) {
        RosterApi rosterApi = RetrofitService.getApi();
        Call<RosterResponse> call = rosterApi.fetchRoster();
        call.enqueue(new Callback<RosterResponse>() {

            @Override
            public void onResponse(Call<RosterResponse> call, Response<RosterResponse> response) {
                if (response.isSuccessful()) {
                    callback.onSuccess(response.body().roster);

                } else {
                    callback.onError();
                }
            }

            @Override
            public void onFailure(Call<RosterResponse> call, Throwable t) {
                callback.onError();
            }

        });
    }

}
