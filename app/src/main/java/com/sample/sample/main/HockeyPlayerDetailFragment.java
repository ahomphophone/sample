package com.sample.sample.main;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.sample.sample.R;
import com.sample.sample.models.HockeyPlayer;
import com.squareup.picasso.Picasso;

public class HockeyPlayerDetailFragment extends AppCompatDialogFragment {

    private static final String HOCKEY_PLAYER_KEY = "hockey_player";

    public static HockeyPlayerDetailFragment newInstance(String hockeyPlayerJson) {
        HockeyPlayerDetailFragment fragment = new HockeyPlayerDetailFragment();
        fragment.setStyle(STYLE_NO_TITLE, 0);
        Bundle args = new Bundle();
        args.putString(HOCKEY_PLAYER_KEY, hockeyPlayerJson);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.hockey_player_detail_view, container);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        String hockeyPlayerJson = getArguments().getString(HOCKEY_PLAYER_KEY);

        Gson gson = new Gson();
        HockeyPlayer hockeyPlayer = gson.fromJson(hockeyPlayerJson, HockeyPlayer.class);

        ImageView profileImageView = (ImageView) view.findViewById(R.id.profileImageView);
        Picasso.with(view.getContext())
                .load(hockeyPlayer.getImageUrl())
                .into(profileImageView);

        TextView nameTextView = (TextView) view.findViewById(R.id.nameTextView);
        nameTextView.setText(hockeyPlayer.getName());

        TextView positionTextView = (TextView) view.findViewById(R.id.positionTextView);
        positionTextView.setText(hockeyPlayer.getPosition());
    }

}
