package com.sample.sample.api;

import retrofit2.Call;
import retrofit2.http.GET;

public interface RosterApi {

    @GET("android_eval.json")
    Call<RosterResponse> fetchRoster();

}
