package com.sample.sample.models;

import com.google.gson.annotations.SerializedName;

public class HockeyPlayer {

    private String name;
    @SerializedName("image_url")
    private String imageUrl;
    private String position;

    public String getName() {
        return name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getPosition() {
        return position;
    }

}
