package com.sample.sample.main;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sample.sample.R;
import com.sample.sample.models.HockeyPlayer;

import java.util.ArrayList;
import java.util.List;

class HockeyPlayerAdapter extends RecyclerView.Adapter<HockeyPlayerViewHolder> {

    interface OnHockeyPlayerClickedListener {
        void onHockeyPlayerClicked(HockeyPlayer hockeyPlayer);
    }

    private List<HockeyPlayer> roster = new ArrayList<>();
    private OnHockeyPlayerClickedListener listener;

    HockeyPlayerAdapter(OnHockeyPlayerClickedListener listener) {
        this.listener = listener;
    }

    @Override
    public HockeyPlayerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.list_item_hockey_player, parent, false);

        return new HockeyPlayerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(HockeyPlayerViewHolder holder, int position) {
        final HockeyPlayer hockeyPlayer = roster.get(position);

        holder.load(hockeyPlayer);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onHockeyPlayerClicked(hockeyPlayer);
            }
        });
    }

    @Override
    public int getItemCount() {
        return roster.size();
    }

    void setRoster(List<HockeyPlayer> roster) {
        this.roster.clear();
        this.roster.addAll(roster);
        notifyDataSetChanged();
    }

    List<HockeyPlayer> getRoster() {
        return roster;
    }

}
