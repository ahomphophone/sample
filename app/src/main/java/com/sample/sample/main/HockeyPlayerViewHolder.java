package com.sample.sample.main;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.sample.sample.R;
import com.sample.sample.models.HockeyPlayer;
import com.squareup.picasso.Picasso;

class HockeyPlayerViewHolder extends RecyclerView.ViewHolder {

    private ImageView profileImageView;
    private TextView nameTextView;
    private TextView positionTextView;

    HockeyPlayerViewHolder(View itemView) {
        super(itemView);

        profileImageView = (ImageView) itemView.findViewById(R.id.profileImageView);
        nameTextView = (TextView) itemView.findViewById(R.id.nameTextView);
        positionTextView = (TextView) itemView.findViewById(R.id.positionTextView);
    }

    void load(HockeyPlayer hockeyPlayer) {
        Picasso.with(profileImageView.getContext())
                .load(hockeyPlayer.getImageUrl())
                .into(profileImageView);

        nameTextView.setText(hockeyPlayer.getName());
        positionTextView.setText(hockeyPlayer.getPosition());
    }

}
